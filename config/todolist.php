<?php
return [
    'model_user' => 'App\\Models\\User',
    'views' =>
        [
            'list' => [
                'edit'  => 'todolist.edit',
                'create'  => 'todolist.create',
                'index' => 'todolist.index'
                ],
            'item' => [
                'edit'  => 'todolist.item.edit',
                'create'  => 'todolist.item.create',
                'index' => 'todolist.item.index'
            ],
            'user' => [
                'edit'  => 'todolist.user.edit',
                'index' => 'todolist.user.index',
                'create'=> 'todolist.user.create',
            ]
        ]
];
