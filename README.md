## Installation notes

- composer install
- configure .env
- php artisan:key generate  
- php artisan:migrate
- php artisan:db seed  
- npm run dev

## How to use

- You can authorize as **user**  using email **user@user.com** and pasword **user** 
- You can authorize as **user1**  using email **user1@user.com** and pasword **user1** 
- You can create your own account


And manage your todo lists
