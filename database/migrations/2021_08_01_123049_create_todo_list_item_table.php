<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTodoListItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('todo_list_item', function (Blueprint $table) {
            $table->id();
            $table->text('text');
            $table->date('finish_until')->nullable();
            $table->boolean('status')->default(false);
            $table->unsignedBigInteger('todo_list_id');
            $table->timestamps();

            $table->foreign('todo_list_id')->references('id')->on('todo_list')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('todo_list_item');
    }
}
