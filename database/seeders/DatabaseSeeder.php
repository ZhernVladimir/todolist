<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Zheravlik\TodoList\Models\ListToUser;
use Zheravlik\TodoList\Models\Role;
use Zheravlik\TodoList\Models\TodoList;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //DB::table('users')->truncate();

        $user = \App\Models\User::create([
            'name' => 'user',
            'email' => 'user@user.com',
            'password' => Hash::make('user'),
            'api_token' => Str::random(60)
        ]);
        $user1 = \App\Models\User::create([
            'name' => 'user1',
            'email' => 'user1@user.com',
            'password' => Hash::make('user1'),
            'api_token' => Str::random(60)
        ]);
        $user2 = \App\Models\User::create([
            'name' => 'user2',
            'email' => 'user2@user.com',
            'password' => Hash::make('user2'),
            'api_token' => Str::random(60)
        ]);

        //DB::table('todo_list_roles')->truncate();

        $role_owner = Role::create(['alias' => 'owner', 'description' => 'List owner']);
        $role_readonly = Role::create(['alias' => 'readonly', 'description' => 'Only for reading']);
        $role_tagonly = Role::create(['alias' => 'tagonly', 'description' => 'Only for managing tags']);
        $role_full = Role::create(['alias' => 'full', 'description' => 'Editing and creating']);

        $lists = collect();
        TodoList::withoutEvents(function() use ($lists){
            $lists->push(TodoList::create([
                'name' => 'list1'
            ]));

            $lists->push(TodoList::create([
                'name' => 'list2'
            ]));
        });

        $list1 = $lists->first();
        $list2 = $lists->last();
        $list1->items()->create([
            'text' => 'item1',
            'finish_until' => Carbon::now(),
            'status' => 0
        ]);
        $list1->items()->create([
            'text' => 'item2',
            'finish_until' => Carbon::now(),
            'status' => 0
        ]);

        $list2->items()->create([
            'text' => 'item3',
            'finish_until' => Carbon::now(),
            'status' => 0
        ]);
        $list2->items()->create([
            'text' => 'item4',
            'finish_until' => Carbon::now(),
            'status' => 0
        ]);

        $list_to_user = new ListToUser();
        $list_to_user->user_id = $user->id;
        $list_to_user->todo_list_id = $list1->id;
        $list_to_user->role_id = $role_owner->id;
        $list_to_user->save();

        $list_to_user = new ListToUser();
        $list_to_user->user_id = $user->id;
        $list_to_user->todo_list_id = $list2->id;
        $list_to_user->role_id = $role_tagonly->id;
        $list_to_user->save();

        $list_to_user = new ListToUser();
        $list_to_user->user_id = $user1->id;
        $list_to_user->todo_list_id = $list2->id;
        $list_to_user->role_id = $role_owner->id;
        $list_to_user->save();

        $list_to_user = new ListToUser();
        $list_to_user->user_id = $user1->id;
        $list_to_user->todo_list_id = $list1->id;
        $list_to_user->role_id = $role_full->id;
        $list_to_user->save();
    }
}
