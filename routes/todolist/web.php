<?php

use Illuminate\Support\Facades\Route;

Route::resource('todo_list',\Zheravlik\TodoList\Http\Controllers\Web\ListController::class);

Route::put('/todo_list/{todo_list}/items/{item}/status', [\Zheravlik\TodoList\Http\Controllers\Web\ItemController::class,'status'])->name('todo_list.items.status');
Route::resource('todo_list.items',\Zheravlik\TodoList\Http\Controllers\Web\ItemController::class);


Route::get('/todo_list/{todo_list}/users/', [\Zheravlik\TodoList\Http\Controllers\Web\UserController::class,'index'])->name('todo_list.users.index');
Route::get('/todo_list/{todo_list}/users/create/', [\Zheravlik\TodoList\Http\Controllers\Web\UserController::class,'create'])->name('todo_list.users.create');
Route::post('/todo_list/{todo_list}/users/store/', [\Zheravlik\TodoList\Http\Controllers\Web\UserController::class,'store'])->name('todo_list.users.store');
Route::delete('/todo_list/{todo_list}/users/{user_id}', [\Zheravlik\TodoList\Http\Controllers\Web\UserController::class,'delete'])->name('todo_list.users.delete');
Route::put('/todo_list/{todo_list}/users/{user_id}', [\Zheravlik\TodoList\Http\Controllers\Web\UserController::class,'update'])->name('todo_list.users.update');
Route::get('/todo_list/{todo_list}/users/{user_id}/edit', [\Zheravlik\TodoList\Http\Controllers\Web\UserController::class,'edit'])->name('todo_list.users.edit');
