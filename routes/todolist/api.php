<?php

use Illuminate\Support\Facades\Route;


Route::get('/', [Zheravlik\TodoList\Http\Controllers\ListController::class, 'index'])->name('todolist.api.index');
Route::get('/{todo_list_id}/edit', [Zheravlik\TodoList\Http\Controllers\ListController::class, 'edit'])->name('todolist.api.edit');
Route::put('/{todo_list_id}/update', [Zheravlik\TodoList\Http\Controllers\ListController::class, 'update'])->name('todolist.api.update');
Route::post('/create', [Zheravlik\TodoList\Http\Controllers\ListController::class, 'store'])->name('todolist.api.store');
Route::delete('/{todo_list_id}/delete', [Zheravlik\TodoList\Http\Controllers\ListController::class, 'delete'])->name('todolist.api.delete');

Route::get('{todo_list_id}/items/', [Zheravlik\TodoList\Http\Controllers\ItemController::class, 'index'])->name('todolist.items.api.index');
Route::post('{todo_list_id}/item/create', [Zheravlik\TodoList\Http\Controllers\ItemController::class, 'store'])->name('todolist.item.api.store');
Route::put('{item_id}/item/update', [Zheravlik\TodoList\Http\Controllers\ItemController::class, 'update'])->name('todolist.item.api.update');
Route::delete('{item_id}/item/delete', [Zheravlik\TodoList\Http\Controllers\ItemController::class, 'delete'])->name('todolist.item.api.delete');
Route::put('{item_id}/item/done', [Zheravlik\TodoList\Http\Controllers\ItemController::class, 'done'])->name('todolist.item.api.done');


Route::get('{todo_list_id}/users/', [Zheravlik\TodoList\Http\Controllers\UserController::class, 'index'])->name('todolist.users.api.index');
Route::post('{todo_list_id}/user/add', [Zheravlik\TodoList\Http\Controllers\UserController::class, 'add'])->name('todolist.user.api.add');
Route::put('{todo_list_id}/user/{user_id}/change-role', [Zheravlik\TodoList\Http\Controllers\UserController::class, 'changeRole'])->name('todolist.user.api.change-role');
Route::delete('{todo_list_id}/user/{user_id}/delete', [Zheravlik\TodoList\Http\Controllers\UserController::class, 'delete'])->name('todolist.user.api.delete');
