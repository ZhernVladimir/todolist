@extends('layouts.todolist')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <todo_lists-component></todo_lists-component>
            </div>
            <div class="col-sm-9">
                <router-view></router-view>
            </div>
        </div>
    </div>



@endsection
