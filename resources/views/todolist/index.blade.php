@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12"><h2>Your todo lists</h2></div>
    </div>
    @foreach($lists as $list)
        <div class="row">
            <div class="col-sm-8">{{ $list->list->name}}</div>
            <div class="col-sm-4">
                <div class="input-group-append">
                    <a class="btn btn-outline-secondary" href="{{route('todo_list.items.index', ['todo_list' => $list->list])}}">Show</a>
                    @can('edit', $list->list )
                    <a class="btn btn-outline-secondary" href="{{route('todo_list.edit', ['todo_list' => $list->list])}}">Edit</a>
                    @endcan
                    @can('delete', $list->list)
                    <form action="{{route('todo_list.destroy', ['todo_list' => $list])}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input class="btn btn-outline-secondary" type="submit" value="Delete">
                    </form>
                    @endcan
                </div>
            </div>
        </div>
    @endforeach
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-10">
            <a class="btn btn-outline-secondary" href="{{route('todo_list.create')}}">Add New</a>
        </div>
    </div>
</div>
@endsection
