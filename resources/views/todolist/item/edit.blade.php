@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                Edit Item
            </div>
        </div>
        <div class="row">
            <form method="POST" action="{{route('todo_list.items.update', ['todo_list' => $todo_list, 'item' => $item])}}">
                @method('PUT')
                @csrf
                <div class="form-group">
                    <label for="text">Text</label>
                    <input type="text" class="form-control" id="text" name="text" value="{{old('text', $item->text)}}"
                           placeholder="Enter text">
                    <small id="emailHelp" class="form-text text-muted">Item text</small>
                </div>
                <div class="form-group">
                    <label for="date">Text</label>
                    <input type="date" class="form-control" id="date" name="finish_until" value="{{old('finish_until', $item->finish_until)}}"
                           placeholder="Enter text">
                    <small class="form-text text-muted"></small>
                </div>
                <div class="form-group">
                    <label for="text">Status</label>
                    <input type="hidden" name="status" value="0">
                    <input type="checkbox" class="form-control" id="status" name="status" {{old('status', $item->status) ? 'checked' : ''}} value="1"
                    >
                    <small class="form-text text-muted"></small>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
                <a class="btn btn-primary" href="{{route('todo_list.items.index', ['todo_list' => $todo_list])}}">Back</a>
                @if($role->alias == 'owner')
                    <a class="btn btn-primary" href="{{route('todo_list.users.index', ['todo_list' => $todo_list])}}">Edit users</a>
                @endif
            </form>
        </div>
    </div>
@endsection
