@extends('layouts.app')
@section('content')

    <div class="container">
        <div class="row ">
            <div class="col-sm-12"><h2>Todo List : {{$todo_list->name}}</h2></div>
        </div>
        @foreach($todo_list->items as $item)
            <div class="row">
                <div class="col-sm-6">{{ $item->text}}</div>
                <div class="col-sm-2">{{ $item->finish_until }}</div>
                <div class="col-sm-4">
                    <div class="input-group-append">
                        @can('edit-item', $todo_list )
                            <a class="btn btn-outline-secondary"
                               href="{{route('todo_list.items.edit', ['todo_list' => $todo_list, 'item' => $item])}}">Edit</a>
                        @endcan
                        @can('delete-item', $todo_list)
                            <form
                                action="{{route('todo_list.items.destroy', ['todo_list' => $todo_list, 'item' => $item])}}"
                                method="POST">
                                @csrf
                                @method('DELETE')
                                <input class="btn btn-outline-secondary" type="submit" value="Delete">
                            </form>
                        @endcan
                        @if(!$item->status )
                            @can('change-status', $todo_list)
                                <form
                                    action="{{route('todo_list.items.status', ['todo_list' => $todo_list, 'item' => $item])}}"
                                    method="POST">
                                    @csrf
                                    @method('PUT')
                                    <input type="hidden" name="status" value="1">
                                    <input class="btn btn-outline-secondary" type="submit" value="Mark as done">
                                </form>
                            @endcan
                        @else
                            <span class="btn btn-outline-secondary">Done.</span>
                        @endif
                    </div>
                </div>
            </div>
        @endforeach
        <div class="row">
            <div class="col-sm-2"></div>
            <div class="col-sm-10">
                @can('edit', $todo_list)
                <a class="btn btn-outline-secondary"
                   href="{{route('todo_list.items.create', ['todo_list' => $todo_list])}}">Add New</a>
                @endif
                <a class="btn btn-outline-secondary" href="{{route('todo_list.index')}}">Back</a>
                @can('manage-users', $todo_list)
                    <a class="btn btn-outline-secondary" href="{{route('todo_list.users.index', compact('todo_list'))}}">Manage users <users></users></a>
                @endcan
            </div>
        </div>
    </div>
@endsection
