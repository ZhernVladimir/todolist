@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                Edit list
            </div>
        </div>
        <div class="row">
            <form method="POST" action="{{route('todo_list.update', $todo_list)}}">
                @method('PUT')
                @csrf
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" id="name" name="name" value="{{old('name', $todo_list->name)}}"  placeholder="Enter name">
                    <small id="emailHelp" class="form-text text-muted">Name of your todo list</small>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
                <a class="btn btn-primary" href="{{route('todo_list.index')}}">Back</a>
            </form>
        </div>

    </div>

@endsection
