@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                Edit user {{$user->name}} ({{$user->email}})
            </div>
        </div>
        <div class="row">
            <form method="POST" action="{{route('todo_list.users.update' , ['todo_list'=> $todo_list, 'user_id' => $user->id]) }}">
                @method('PUT')
                @csrf
                <div class="form-group">
                    <label for="status">Status</label>
                    <select name="status" id="status">
                        <option value="readonly" {{ old('status', $role->alias) == 'readonly' ? 'selected' : ''}}> Read only</option>
                        <option value="tagonly" {{ old('status', $role->alias) == 'tagonly' ? 'selected' : ''}}>Only status change</option>
                        <option value="full" {{ old('status', $role->alias) == 'full' ? 'selected' : ''}}>Full access</option>
                    </select>
                    <small  class="form-text text-muted">New  user status</small>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
                <a class="btn btn-primary" href="{{route('todo_list.users.index' , compact('todo_list'))}}">Back</a>
            </form>
        </div>
    </div>
@endsection
