@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12"><h2>Users of {{ $todo_list->name }}</h2></div>
        </div>
        @foreach($list_to_users as $list_to_user)
            <div class="row">
                <div class="col-sm-4">{{ $list_to_user->user->name}} ({{$list_to_user->user->email}})}</div>
                <div class="col-sm-4">{{ $list_to_user->role->description}}</div>
                <div class="col-sm-4">
                    <div class="input-group-append">

                        @if($list_to_user->user->id != auth()->user()->id)
                            <a class="btn btn-outline-secondary" href="{{route('todo_list.users.edit', ['todo_list' => $todo_list, 'user_id' => $list_to_user->user->id])}}">Edit</a>


                            <form action="{{route('todo_list.users.delete', ['todo_list' => $todo_list, 'user_id' => $list_to_user->user->id])}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input class="btn btn-outline-secondary" type="submit" value="Delete">
                            </form>
                        @endif
                    </div>
                </div>
            </div>
        @endforeach
        <div class="row">
            <div class="col-sm-2"></div>
            <div class="col-sm-10">
                <a class="btn btn-outline-secondary" href="{{route('todo_list.items.index', compact('todo_list'))}}">Back</a>
                <a class="btn btn-outline-secondary" href="{{route('todo_list.users.create', compact('todo_list'))}}">Add</a>
            </div>
        </div>
    </div>
@endsection
