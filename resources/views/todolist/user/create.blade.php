@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                Add user
            </div>
        </div>

        <div class="row">
            <form method="POST" action="{{route('todo_list.users.store' , compact('todo_list'))}}">
                @method('POST')
                @csrf
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="text" class="form-control" id="email" name="email" value="{{old('email')}}"
                           placeholder="Enter name">
                    <small  class="form-text text-muted">New user email</small>
                </div>
                <div class="form-group">
                    <label for="status">Status</label>
                    <select name="status" id="status">
                        <option value="readonly" checked>Read only</option>
                        <option value="tagonly">Only status change</option>
                        <option value="full">Full access</option>
                    </select>
                    <small  class="form-text text-muted">New  user status</small>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
                <a class="btn btn-primary" href="{{route('todo_list.users.index' , compact('todo_list'))}}">Back</a>
            </form>
        </div>
    </div>
@endsection
