<?php

namespace Zheravlik\TodoList\Exceptions;

use Exception;

class UserNotConnectedToListException extends Exception
{
    //
}
