<?php

namespace Zheravlik\TodoList\Exceptions;

use Exception;

class UserAlreadyConnectedToTodoListException extends Exception
{
    //
}
