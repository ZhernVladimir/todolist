<?php

namespace Zheravlik\TodoList\Observers;


use Zheravlik\TodoList\Contracts\Manager;
use Zheravlik\TodoList\Models\Role;
use Zheravlik\TodoList\Models\ListToUser;
use Zheravlik\TodoList\Models\TodoList;

class TodoListObserver
{
    public function deleting(TodoList $list)
    {
        $list->to_user()->delete();
    }

    public function created(TodoList  $list){
        $manager  = app()->make( Manager::class);
        $list_to_user  = new ListToUser();
        $list_to_user->user_id =  $manager->getTodoListManagerId();
        $list_to_user->role_id = Role::where('alias', 'owner')->get()->first()->id;
        $list->to_user()->save($list_to_user);
    }
}
