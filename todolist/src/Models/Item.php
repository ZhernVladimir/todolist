<?php

namespace Zheravlik\TodoList\Models;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $table = 'todo_list_item';
    protected $fillable = ['text', 'finish_until','status', 'todo_list_id'];

    public function list()
    {
        return $this->belongsTo(TodoList::class, 'todo_list_id');
    }
}
