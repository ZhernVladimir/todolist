<?php

namespace Zheravlik\TodoList\Models;

use Illuminate\Database\Eloquent\Model;

class TodoList extends Model
{
    protected $table = 'todo_list';
    protected $fillable = ['name'];

    public function to_user(){
        return $this->hasMany(ListToUser::class, 'todo_list_id');
    }

    public function items(){
        return $this->hasMany(Item::class, 'todo_list_id');
    }
}
