<?php

namespace Zheravlik\TodoList\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'todo_list_roles';
    protected $fillable = ['alias', 'description'];
}
