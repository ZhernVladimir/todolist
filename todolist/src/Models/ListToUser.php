<?php

namespace Zheravlik\TodoList\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

class ListToUser extends Model
{
    protected $table = 'todo_list_users';
    protected $fillable = ['user_id', 'role_id', 'todo_list_id'];

    public function user()
    {
        return $this->belongsTo(Config::get('todolist.model_user'), 'user_id');
    }

    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id');
    }

    public function list()
    {
        return $this->belongsTo(TodoList::class, 'todo_list_id');
    }
}
