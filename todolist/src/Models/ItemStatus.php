<?php

namespace Zheravlik\TodoList\Models;

use Illuminate\Database\Eloquent\Model;

class ItemStatus extends Model
{
    protected $table = 'todo_list_item_status';

    protected $fillable = ['alias', 'description'];
}
