<?php


namespace Zheravlik\TodoList\Traits;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Zheravlik\TodoList\Models\ListToUser;

trait TodoListManager
{
    protected $loaded_todo_list_roles = [];

    public function getTodoListManagerId() : int
    {
        return (int) $this->id;
    }
    public function todoLists() : HasMany
    {
        return $this->hasMany(ListToUser::class, 'user_id');
    }

    public function todoListRole($list_id) : ?string
    {
        if(!isset($this->loaded_todo_list_roles[$list_id])){
            $list_to_user  = $this->todoLists()->where('todo_list_id', $list_id)->with('role')->first();
            $this->loaded_todo_list_roles[$list_id]  = $list_to_user->role->alias ?? null;
        }

        return $this->loaded_todo_list_roles[$list_id] ?? null;
    }
}
