<?php

namespace Zheravlik\TodoList\Provider;

use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Route;
use Zheravlik\TodoList\Contracts\Manager;
use Zheravlik\TodoList\Observers\TodoListObserver;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use Zheravlik\TodoList\Models\TodoList;
use Zheravlik\TodoList\Policies\TodoListPolicy;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Manager::class, function ($app) {
            return auth()->user();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        TodoList::observe(TodoListObserver::class);
        Gate::policy(TodoList::class, TodoListPolicy::class);
    }
}
