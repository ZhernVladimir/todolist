<?php


namespace Zheravlik\TodoList\Contracts;

use Illuminate\Database\Eloquent\Relations\HasMany;

interface Manager
{
    public function getTodoListManagerId():int;
    public function todoLists():HasMany;
    public function todoListRole($list_id): ?string;
}
