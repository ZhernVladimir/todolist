<?php

namespace Zheravlik\TodoList\Policies;

use Zheravlik\TodoList\Contracts\Manager;
use Zheravlik\TodoList\Models\TodoList;
use Zheravlik\TodoList\Models\Item;
use Zheravlik\TodoList\Models\ListToUser;
use Illuminate\Auth\Access\HandlesAuthorization;

class TodoListPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function view( $user, TodoList $list)
    {
        /** @var Manager $user */
        $role  = $user->todoListRole($list->id);
        return (bool) $role;
    }


    public function createItem($user, TodoList $list)
    {
        /** @var Manager $user */
        $role  = $user->todoListRole($list->id);
        return $role && in_array($role, ['owner',  'full'] );
    }


    public function editItem($user, TodoList $list)
    {
        /** @var Manager $user */
        $role  = $user->todoListRole($list->id);
        return $role && in_array($role, ['owner',  'full'] );
    }

    public function edit($user, TodoList $list)
    {
        /** @var Manager $user */
        $role  = $user->todoListRole($list->id);
        return $role && in_array($role, ['owner',  'full'] );
    }

    public function deleteItem($user, TodoList $list)
    {
        /** @var Manager $user */
        $role  = $user->todoListRole($list->id);
        return $role && in_array($role, ['owner', 'full'] );
    }

    public function delete($user, TodoList $list)
    {
        /** @var Manager $user */
        $role  = $user->todoListRole($list->id);
        return $role && $role == 'owner';
    }

    public function manageUsers($user, TodoList $list)
    {
        /** @var Manager $user */
        $role  = $user->todoListRole($list->id);
        return $role && $role == 'owner';
    }

    public function changeStatus($user, TodoList $list)
    {
        /** @var Manager $user */
        $role  = $user->todoListRole($list->id);
        return  in_array($role, ['owner', 'tagonly', 'full'] );
    }

}
