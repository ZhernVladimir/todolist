<?php

namespace Zheravlik\TodoList\Http\Requests;

use Illuminate\Support\Facades\Config;
use Illuminate\Foundation\Http\FormRequest;

class DisconnectUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'list_id' => 'required|int|exists:Zheravlik\\TodoList\\Models\\TodoList,id',
            'user_id' => 'required|int|exists:'. Config::get('todolist.user_class'). ',id',
        ];
    }
}
