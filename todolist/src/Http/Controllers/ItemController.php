<?php

namespace Zheravlik\TodoList\Http\Controllers;

use Zheravlik\TodoList\Contracts\Manager;
use Zheravlik\TodoList\Http\Requests\CreateItemRequest;
use Zheravlik\TodoList\Http\Requests\DeleteItemRequest;
use Zheravlik\TodoList\Http\Requests\EditItemRequest;
use Zheravlik\TodoList\Http\Requests\UpdateItemApiRequest;
use Zheravlik\TodoList\Models\Item;
use Zheravlik\TodoList\Models\TodoList;


class ItemController extends Controller
{
    public function index($todo_list_id, Manager $manager)
    {
        $todo_list = TodoList::findOrfail($todo_list_id);
        $this->authorize('view', $todo_list);
        $todo_list_user = $manager->todoLists()->where('todo_list_id', $todo_list->id)->with('role')->get()->first();

        return [
            'title' => $todo_list->name,
            'items' => $todo_list->items()->get(),
            'role' => $todo_list_user->role->alias
        ];
    }

    public function store($todo_list_id ,CreateItemRequest $request)
    {
        $list = TodoList::findOrFail($todo_list_id);
        $this->authorize('create-item', $list);
        Item::create([
            'todo_list_id' => $todo_list_id,
            'text' => $request->text,
            'finish_until' => $request->finish_until
        ]);
        return response()->json(['success' => true]);
    }

    public function update($item_id ,UpdateItemApiRequest $request)
    {
        $item = Item::with('list')->findOrFail($item_id);
        $this->authorize('edit-item', $item->list);
        $item->update($request->validated());
        return response()->json(['success' => true]);
    }

    public function done($item_id)
    {
        $item = Item::with('list')->findOrFail($item_id);
        $this->authorize('change-status', $item->list);
        $item->status =1;
        $item->save();
        return response()->json(['success' => true]);
    }

    public function delete($item_id)
    {
        $item = Item::with(['list'])->find($item_id);
        $this->authorize('delete-item', $item->list);
        $item->delete();
        return response()->json(['success' => true]);
    }
}
