<?php

namespace Zheravlik\TodoList\Http\Controllers\Web;

use Illuminate\Support\Facades\Config;
use Zheravlik\TodoList\Contracts\Manager;
use Zheravlik\TodoList\Http\Controllers\Controller;
use Zheravlik\TodoList\Http\Requests\CreateTodoListRequest;
use Zheravlik\TodoList\Http\Requests\DeleteTodoListRequest;
use Zheravlik\TodoList\Http\Requests\UpdateListRequest;
use Zheravlik\TodoList\Models\TodoList;

class ListController extends Controller
{
    public function index(Manager $manager)
    {
        $lists = $manager->todoLists()->with(['list', 'role'])->get();
        return view(Config::get('todolist.views.list.index') , compact('lists'));
    }


    public function create()
    {
        return view(Config::get('todolist.views.list.create') );
    }

    public function edit(TodoList $todo_list)
    {
        $this->authorize('edit', $todo_list);
        return view(Config('todolist.views.list.edit'), compact('todo_list') );
    }

    public function store(CreateTodoListRequest $request)
    {
        $todo_list = TodoList::create($request->validated());
        return redirect()->route('todo_list.index')->with(['message' => 'List created successfully']);
    }

    public function update(TodoList $todo_list, UpdateListRequest $request)
    {
        $this->authorize('edit' , $todo_list);
        $todo_list->update($request->validated());
        return redirect()->route('todo_list.edit', ['todo_list' => $todo_list]);
    }

    public function destroy( TodoList $todo_list){
        $this->authorize('delete',$todo_list);
        $todo_list->delete();
        return redirect()->route('todo_list.index')->with('message', 'Deleted successfully');
    }
}
