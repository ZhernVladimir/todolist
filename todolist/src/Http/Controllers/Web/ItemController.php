<?php


namespace Zheravlik\TodoList\Http\Controllers\Web;

use Illuminate\Support\Facades\Config;
use Zheravlik\TodoList\Contracts\Manager;
use Zheravlik\TodoList\Http\Controllers\Controller;
use Zheravlik\TodoList\Http\Requests\CreateItemRequest;
use Zheravlik\TodoList\Http\Requests\UpdateItemRequest;
use Zheravlik\TodoList\Http\Requests\UpdateItemStatusRequest;
use Zheravlik\TodoList\Models\Item;
use Zheravlik\TodoList\Models\TodoList;

class ItemController extends Controller
{
    public function index(TodoList $todo_list)
    {
        $this->authorize('view', $todo_list);
        $todo_list->load(['items']);
        /** @var Manager $manager */
        $manager = app()->make(Manager::class);
        $role = $todo_list->to_user()->where('user_id', $manager->getTodoListManagerId())->with(['role'])->first()->role;

        return view(Config::get('todolist.views.item.index'), compact('todo_list','role'));
    }

    public function create(TodoList $todo_list)
    {
        $this->authorize('create-item', $todo_list);
        return view(Config::get('todolist.views.item.create'), compact('todo_list'));
    }

    public function store( CreateItemRequest $request, TodoList $todo_list)
    {
        $this->authorize('create-item', $todo_list);
        $todo_list->items()->save(new Item($request->validated()));

        return redirect()->route('todo_list.items.index' , compact('todo_list'))->with(['message' => 'Item created successfully']);
    }

    public function edit(TodoList $todo_list, Item $item, Manager $manager)
    {
        $this->authorize('edit-item', $todo_list);
        $role = $manager->todoLists()->where('todo_list_id', $todo_list->id)->first()->role;
        return view(Config::get('todolist.views.item.edit'), compact('todo_list', 'item', 'role'));
    }

    public function update(TodoList $todo_list, Item $item, UpdateItemRequest $request){
        $this->authorize('edit-item', $todo_list);
        $item->update($request->validated());
        return redirect()->route('todo_list.items.index' , compact('todo_list'))->with(['message', 'Updated successfully']);
    }

    public function status(TodoList $todo_list, Item $item, UpdateItemStatusRequest $request){
        abort_if(!$todo_list->items()->find($item->id), 404 );
        $this->authorize('change-status', $todo_list);
        $item->status = $request->status;
        $item->save();
        return back()->with(['message' => 'Status changed']);
    }
}
