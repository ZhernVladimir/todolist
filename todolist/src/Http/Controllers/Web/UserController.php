<?php

namespace Zheravlik\TodoList\Http\Controllers\Web;

use Illuminate\Support\Facades\Config;
use Zheravlik\TodoList\Contracts\Manager;
use Zheravlik\TodoList\Exceptions\UserAlreadyConnectedToTodoListException;
use Zheravlik\TodoList\Http\Controllers\Controller;
use Zheravlik\TodoList\Http\Requests\ConnectUserRequest;
use Zheravlik\TodoList\Http\Requests\CreateTodoListRequest;
use Zheravlik\TodoList\Http\Requests\DeleteTodoListRequest;
use Zheravlik\TodoList\Http\Requests\UpdateListRequest;
use Zheravlik\TodoList\Http\Requests\UpdateUserStatusRequest;
use Zheravlik\TodoList\Models\ListToUser;
use Zheravlik\TodoList\Models\Role;
use Zheravlik\TodoList\Models\TodoList;

class UserController extends Controller
{
    public function index(TodoList $todo_list)
    {
        $this->authorize('manage-users', $todo_list);
        $list_to_users = $todo_list->to_user()->with('role' ,'user')->get();
        return view(Config::get('todolist.views.user.index') , compact('todo_list', 'list_to_users'));
    }

    public function create(TodoList $todo_list)
    {
        $this->authorize('manage-users', $todo_list);
        return view(Config::get('todolist.views.user.create') , compact('todo_list'));
    }

    public function store(TodoList $todo_list, ConnectUserRequest $request)
    {
        $this->authorize('manage-users', $todo_list);
        $UserModel = Config::get('todolist.model_user');
        $user = $UserModel::where('email' , $request->email)->first();
        if(!$user) return back()->withErrors(['no_user' => 'No user with this email founded']);

        if(ListToUser::where('user_id',$user->id)->where('todo_list_id', $todo_list->id)->first()){
            //throw new UserAlreadyConnectedToTodoListException();
            if(!$user) return back()->withErrors(['already_connected' => 'User already connected to list']);
        }

        $role = Role::where('alias',$request->status)->first();

        $list_to_user = new ListToUser();
        $list_to_user->user_id = $user->id;
        $list_to_user->role_id = $role->id;
        $list_to_user->todo_list_id = $todo_list->id;
        $list_to_user->save();

        return redirect()->route('todo_list.users.index', compact('todo_list'))->with(['message' =>  'User added to list successfully']);
    }

    public function update(UpdateUserStatusRequest $request, TodoList $todo_list, $user_id)
    {
        $this->authorize('manage-users', $todo_list);
        $UserModel = Config::get('todolist.model_user');
        $user = $UserModel::findOrFail($user_id);
        $list_to_user = ListToUser::where('user_id', $user_id)->where('todo_list_id', $todo_list->id)->with('role')->first();
        if(!$list_to_user)  return back()->withErrors(['not_connected' => 'User not connected to list']);
        $list_to_user->role_id = Role::where('alias', $request->status)->first()->id;
        $list_to_user->save();
        return redirect()->route('todo_list.users.index', compact('todo_list'))->with(['message' =>  'User status updated successfully']);
    }

    public function edit(TodoList $todo_list, $user_id)
    {
        $this->authorize('manage-users', $todo_list);
        $UserModel = Config::get('todolist.model_user');
        $user = $UserModel::findOrFail($user_id);
        $role = ListToUser::where('user_id', $user_id)->where('todo_list_id', $todo_list->id)->with('role')->first()->role;
        return view(Config::get('todolist.views.user.edit') , compact('todo_list', 'user', 'role'));
    }

    public function delete(TodoList $todo_list, $user_id)
    {
        $this->authorize('manage-users', $todo_list);
        $list_to_user = ListToUser::where('user_id', $user_id)->where('todo_list_id', $todo_list->id)->firstOrFail();
        $list_to_user->delete();
        return back()->with(['message' => 'User disconnected from list']);
    }

}
