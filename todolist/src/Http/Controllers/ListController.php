<?php

namespace Zheravlik\TodoList\Http\Controllers;

use Zheravlik\TodoList\Contracts\Manager;
use Zheravlik\TodoList\Http\Requests\CreateTodoListRequest;
use Zheravlik\TodoList\Http\Requests\DeleteTodoListRequest;

use Zheravlik\TodoList\Http\Requests\UpdateListRequest;
use Zheravlik\TodoList\Models\TodoList;

class ListController extends Controller
{
    public function index(Manager $manager)
    {
        return $manager->todoLists()->with(['list', 'role'])->get()->map(function($item){
            return [
                'id' => $item->list->id,
                'name' => $item->list->name,
                'role' => $item->role->alias
            ];
        });
    }

    public function update($todo_list_id, UpdateListRequest $request)
    {
        $todo_list = TodoList::findOrFail($todo_list_id);
        $this->authorize('edit',$todo_list );
        $todo_list->update($request->validated());
        return response()->json(['success' => true]);
    }

    public function store(CreateTodoListRequest $request)
    {
        TodoList::create($request->validated());
    }

    public function delete($todo_list_id){
        $todo_list = TodoList::findOrFail($todo_list_id);
        $this->authorize('delete',$todo_list);
        $todo_list->delete();
    }
}
