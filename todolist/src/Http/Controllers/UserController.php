<?php

namespace Zheravlik\TodoList\Http\Controllers;

use Illuminate\Support\Facades\Config;
use Zheravlik\TodoList\Contracts\Manager;
use Zheravlik\TodoList\Exceptions\CantDisconnectOwnerException;
use Zheravlik\TodoList\Exceptions\UserAlreadyConnectedToTodoListException;
use Zheravlik\TodoList\Exceptions\UserNotConnectedToListException;
use Zheravlik\TodoList\Http\Requests\ConnectUserApiRequest;
use Zheravlik\TodoList\Http\Requests\ChangeUserRoleRequest;
use Zheravlik\TodoList\Http\Requests\DisconnectUserRequest;
use Zheravlik\TodoList\Http\Requests\UpdateUserStatusRequest;
use Zheravlik\TodoList\Models\ListToUser;
use Zheravlik\TodoList\Models\TodoList;
use Zheravlik\TodoList\Models\Role;

class UserController extends Controller
{
    public function index($todo_list_id)
    {
        /** @var Manager $manager */
        $todo_list = TodoList::findOrFail($todo_list_id);
        $manager = app()->make(Manager::class);
        $this->authorize('manage-users', $todo_list);
        $list = $todo_list->to_user()->with(['role', 'user'])->get()->filter(function ($list_to_user) use ($manager) {
            return $list_to_user->user->id != $manager->getTodoListManagerId();
        })->map(function ($list_to_user) {
            return [
                'name' => $list_to_user->user->name,
                'email' => $list_to_user->user->email,
                'id' => $list_to_user->user->id,
                'role' => $list_to_user->role->alias
            ];
        });

        return compact('list');
    }

    public function add($todo_list_id, ConnectUserApiRequest $request)
    {

        $todo_list = TodoList::findOrFail($todo_list_id);
        $this->authorize('manage-users', $todo_list);

        $UserModel = Config::get('todolist.model_user');
        $user = $UserModel::where('email', $request->input('email'))->first();
        if(!$user){
            return response()->json(['success' => false, 'massage' => 'User not found' ]);
        }

        if (ListToUser::where('user_id', $user->id)->where('todo_list_id', $todo_list_id)->exists()) {
            throw new UserAlreadyConnectedToTodoListException();
        }
        $role = Role::where('alias', $request->input('status'))->firstOrFail();
        ListToUser::create([
            'user_id' => $user->id,
            'todo_list_id' => $todo_list_id,
            'role_id' => $role->id,
        ]);
        return response()->json(['success' => true]);
    }

    public function changeRole($todo_list_id,$user_id, UpdateUserStatusRequest $request)
    {
        $todo_list = TodoList::findOrFail($todo_list_id);
        $this->authorize('manage-users', $todo_list);

        $list_to_user = $todo_list->to_user()->where('user_id', $user_id)->first();
        if (!$list_to_user) {
            throw new UserNotConnectedToListException();
        }

        $role = Role::where('alias', $request->input('status') )->firstOrFail();
        $list_to_user->role_id = $role->id;
        $list_to_user->save();
        return response()->json(['success' => true]);
    }

    public function delete( $todo_list_id, $user_id)
    {
        $todo_list = TodoList::findOrFail($todo_list_id);
        $this->authorize('manage-users', $todo_list);

        $list_to_user = $todo_list->to_user()->where('user_id', $user_id)->first();
        if (!$list_to_user) {
            throw new UserNotConnectedToListException();
        }

        /** @var Manager $manager */
        $manager = app()->make(Manager::class);
        if($user_id == $manager->getTodoListManagerId()){
            throw new CantDisconnectOwnerException();
        }

        $list_to_user->delete();
        return response()->json(['success' => true]);
    }
}
